<?php
include "../koneksi/koneksi.php";
		//jika sudah mendapatkan parameter GET id dari URL
		?>
<html>
<head>
  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->


</head>
<body>
	 <!-- Begin PNama User Content -->
 <div class="container-fluid">

<!-- PNama User Heading -->
<h1 class="h3 mb-2 text-gray-800">Data Reseller</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Data Detail reseller</h6>
  </div>
  <div class="card-body">
	<div class="table-responsive">
	
	  <table class="table table-hover" id="example" width="100%" cellspacing="0">
	  <caption>Data Detail reseller</caption>
		<thead>
		  <tr>
			<th>No</th>
			<th>Id Detail</th>
			<th>Jenis Bahan</th>
			<th>Qty</th>
			<th>Harga Satuan</th>
			<th>jumlah</th>
		  </tr>
		</thead>
                  <tbody>
				  <?php 
				   include "../koneksi/koneksi.php";
				   //jika sudah mendapatkan parameter GET id dari URL
				   if(isset($_GET['id_session'])){
					   //membuat variabel $id untuk menyimpan id dari GET id di URL
					   $id_session = $_GET['id_session'];
					   
					   //query ke database SELECT tabel mahasiswa berdasarkan id = $id
					   $select = mysqli_query($db, "SELECT * FROM reseller_detail WHERE id_session='$id_session'") or die(mysqli_error($db));
					   
					   //jika hasil query = 0 maka muncul pesan error
					   if(mysqli_num_rows($select) == 0){
						   echo '<div class="alert alert-warning">Data tidak ada dalam database.</div>';
						   exit();
					   //jika hasil query > 0
					   }else{
						   //membuat variabel $data dan menyimpan data row dari query
						   $data = mysqli_fetch_assoc($select);
					   }
				   
				 // $hasil=mysqli_query($db,$sql);
				 $no=0;
				 // while ($data = mysqli_fetch_array($hasil)) {
					 $no++;
			
						//menampilkan data perulangan
						echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$data['id_session'].'</td>
							<td>'.$data['jenis_bahan'].'</td>
							<td>'.$data['qty'].'</td>
							<td>'.$data['harga'].'</td>
							<td>'.$data['jumlah'].'</td>
						</tr>
						';
						$no++;
					}
				
				//jika query menghasilkan nilai 0
	
				?>
				
		</tbody>
		<tfoot>
	  </table>
	 
	  <td colspan="5" align="center">Total :</td>
 <td><?php
			 
					 include "../koneksi/koneksi.php";
					 $query = mysqli_query($db, "SELECT SUM(jumlah) AS total FROM `reseller_detail` WHERE id_session='$id_session'") or die(mysqli_error());
					 $fetch = mysqli_fetch_array($query);
			 ?>
				 <label class="text-danger"><?php echo '<a> Rp.' . number_format($fetch['total'], 0, ".", ".").'</a>'?></label>
			 </td>

<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>



	</div>
  </div>

</div>
</div>


				</body>
				<html>