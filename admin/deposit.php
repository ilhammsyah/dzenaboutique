
<head>
  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
 
 
 <script src="../js/jquery-3.1.0.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
 

</head>
<body>

<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Detail Stok Masuk</h6>
  </div>
  <div class="card-body">
	<div class="table-responsive">
	  <table class="table table-hover" id="example" width="100%" cellspacing="0">
	  <caption>Data Stok Masuk</caption>
		<thead>
		  <tr>
			<th>No</th>
			<th>Kode Stok Masuk</th>
			<th>Kode Barang</th>
			<th>Stok Masuk</th>
			<th>Tanggal Masuk</th>
			<th>Action</th>
		  </tr>
		</thead>
                  <tbody>
                  <?php
                   include "../koneksi/koneksi.php";
				//query ke database SELECT tabel mahasiswa urut berdasarkan id yang paling besar
				$sql = mysqli_query($db, "SELECT * FROM stok_masuk") or die(mysqli_error($db));
				//jika query diatas menghasilkan nilai > 0 maka menjalankan script di bawah if...
				if(mysqli_num_rows($sql) > 0){
					//membuat variabel $no untuk menyimpan nomor urut
					$no = 1;
					//melakukan perulangan while dengan dari dari query $sql
					while($data = mysqli_fetch_assoc($sql)){
						//menampilkan data perulangan
						echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$data['id'].'</td>
							<td>'.$data['kode_barang'].'</td>
							<td>'.$data['stok_masuk'].'</td>
							<td>'.$data['tanggal_masuk'].'</td>
							<td>
							<a href="stok_hapus.php?id='.$data['id'].'" class="badge badge-danger" onclick="return confirm(\'Yakin ingin menghapus data ini?\')">Delete</a>
							</td>
						</tr>
						';
						$no++;
					}
				//jika query menghasilkan nilai 0
				}else{
					echo '
					<tr>
						<td colspan="6">Tidak ada data.</td>
					</tr>
					';
				}
				?>
		</tbody>
		<tfoot>
	  </table>


  

<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>



	</div>
  </div>	


<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Detail Stok Keluar</h6>
  </div>
  <div class="card-body">
	<div class="table-responsive">
	  <table class="table table-hover" id="example" width="100%" cellspacing="20">
	  <caption>Data barang</caption>
		<thead>
		  <tr>
			<th>No</th>
			<th>Id Pesanan</th>
			<th>Stok Keluar</th>
			<th>Tanggal Keluar</th>
			<th>Action</th>
		  </tr>
		</thead>
		<tfoot>
                <tr>
                     <th>No</th>
			<th>Id Pesanan</th>
			<th>Stok Keluar</th>
			<th>Tanggal Keluar</th>
			<th>Action</th>
                </tr>
                  </tfoot>
                  <tbody>
                  <?php
                   include "../koneksi/koneksi.php";
				//query ke database SELECT tabel mahasiswa urut bErdasarkan id yang paling besar
				$sql = mysqli_query($db, "SELECT * FROM stok_keluar") or die(mysqli_error($koneksi));
				//jika query diatas menghasilkan nilai > 0 maka menjalankan script di bawah if...
				if(mysqli_num_rows($sql) > 0){
					//membuat variabel $no untuk menyimpan nomor urut
					$no = 1;
					//melakukan perulangan while dengan dari dari query $sql
					while($data = mysqli_fetch_assoc($sql)){
						//menampilkan data perulangan
						echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$data['id_pesanan'].'</td>
							<td>'.$data['stok_masuk'].'</td>
							<td>'.$data['tanggal_masuk'].'</td>
							<td>
							<a href="barang_hapus.php?id='.$data['id'].'" class="badge badge-danger" onclick="return confirm(\'Yakin ingin menghapus data ini?\')">Delete</a>
							</td>
						</tr>
						';
						$no++;
					}
				//jika query menghasilkan nilai 0
				}else{
					echo '
					<tr>
						<td colspan="6">Tidak ada data.</td>
					</tr>
					';
				}
				?>
		</tbody>
		<tfoot>
	  </table>


  

<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>



	</div>
  </div>
</div>
</div>

<script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page jenis_barang plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page jenis_barang custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
				</body>