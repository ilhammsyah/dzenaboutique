<html>
<head>
  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
 
 
 <script src="../js/jquery-3.1.0.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
 

</head>
<body>
	 <!-- Begin PNama User Content -->
 <div class="container-fluid">

<!-- PNama User Heading -->
<h1 class="h3 mb-2 text-gray-800">Data Omzet</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Data Omzet</h6>
  </div>
  <div class="card-body">
	<div class="table-responsive">
	<!-- <form action="" method="post">
			<label>PILIH TANGGAL</label>
			<input type="date" name="tanggal">
			<input type="submit" value="FILTER">
		</form> -->
 
	
	  <table class="table table-hover" id="example" width="100%" cellspacing="0">
	  <caption>Data Omzet</caption>
		<thead>
		  <tr>
			<th>No</th>
			<th>No Resi</th>
			<th>Nama Pemesan</th>
			<th>Tanggal</th>
			<th>Total</th>
		  </tr>
		</thead>
                  <tbody>
				  <?php 
			//untuk meinclude kan koneksi
			include('../koneksi/koneksi.php');

				//jika kita klik cari, maka yang tampil query cari ini
				if(isset($_POST['submit'])) {
					//menampung variabel kata_cari dari form pencarian
					$cari = $_POST['tanggal'];

					//jika hanya ingin mencari berdasarkan kode_produk, silahkan hapus dari awal OR
					//jika ingin mencari 1 ketentuan saja query nya ini : SELECT * FROM produk WHERE kode_produk like '%".$kata_cari."%' 
					$query = "SELECT * FROM pesanan WHERE tanggal LIKE '%$cari%'";
				} else {
					//jika tidak ada pencarian, default yang dijalankan query ini
					$query = "SELECT * FROM pesanan";
				}
				$no = 1;

				$result = mysqli_query($db, $query);

				if(!$result) {
					die("Query Error : ".mysqli_errno($db)." - ".mysqli_error($db));
				}
				//kalau ini melakukan foreach atau perulangan
				while ($row = mysqli_fetch_assoc($result)) {
			
						//menampilkan data perulangan
						echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$row['no_resi'].'</td>
							<td>'.$row['nm_pemesan'].'</td>
							<td>'.$row['tanggal'].'</td>
							<td>'.$row['total'].'</td>
						</tr>
						';
						$no++;
					}
				//jika query menghasilkan nilai 0
	
				?>
		</tbody>
		<tfoot>
		
	  </table>
	  <div>
	  <td colspan="7" align="">Total :</td>
<td><?php
			
					include "../koneksi/koneksi.php";
					$query = mysqli_query($db, "SELECT SUM(total) AS total FROM pesanan") or die(mysqli_error());
					$fetch = mysqli_fetch_array($query);
			?>
				<label class="text-danger"><?php echo '<a> Rp.' . number_format($fetch['total'], 0, ".", ".").'</a>'?></label>
			</td>
				</div>
  

<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>



	</div>
  </div>

</div>
</div>


  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page jenis_reseller plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page jenis_reseller custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
				</body>
				<html>