<?php
include('../koneksi/koneksi.php');
$label = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

for($bulan = 1;$bulan < 13;$bulan++)
{
	$query = mysqli_query($db,"select sum(total) as total from pesanan where MONTH(tanggal)='$bulan'");
	$row = $query->fetch_array();
	$jumlah_produk[] = $row['total'];
}
?>



<!-- Menu -->
<!-- <div class="col-xl-3 col-md-7 mb-5">
<div class="card border-left-success shadow h-100 py-2">
 <div class="card-body">
   <div class="row no-gutters align-items-center">
     <div class="col mr-2">
       <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Data barang</div>
       <a href="?page=lihatbarang" class="text-xs font-weight-bold text-danger">Lihat</a>
       <div class="h5 mb-0 font-weight-bold text-gray-800">
       <?php 
      //  include "../koneksi/koneksi.php";
      //  $sql = mysqli_query($db,"SELECT * FROM barang");
      //  $count = mysqli_num_rows($sql);
      //  echo "$count"; ?></div>
     </div>
     <div class="col-auto">
       <i class="fas fa-book fa-2x text-gray-300"></i>
     </div>
   </div>
 </div>
</div>
</div> -->

<!-- Transaksi -->
<!-- <div class="col-xl-3 col-md-7 mb-5">
<div class="card border-left-primary shadow h-100 py-2">
 <div class="card-body">
   <div class="row no-gutters align-items-center">
     <div class="col mr-2">
       <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data Reseller</div>
       <a href="?page=lihatpesanan" class="text-xs font-weight-bold text-danger">Lihat</a>
       <div class="h5 mb-0 font-weight-bold text-gray-800">
       <?php 
      //  include "../koneksi/koneksi.php";
      //  $sql = mysqli_query($db,"SELECT * FROM reseller");
      //  $count = mysqli_num_rows($sql);
      //  echo "$count"; ?></div>
     </div>
     <div class="col-auto">
       <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
     </div>
   </div>
 </div>
</div>
</div> -->


<!-- Transaksi -->
<!-- <div class="col-xl-3 col-md-7 mb-5">
<div class="card border-left-primary shadow h-100 py-2">
 <div class="card-body">
   <div class="row no-gutters align-items-center">
     <div class="col mr-2">
       <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data Pesanan</div>
       <a href="?page=lihatpesanan" class="text-xs font-weight-bold text-danger">Lihat</a>
       <div class="h5 mb-0 font-weight-bold text-gray-800">
       <?php 
      //  include "../koneksi/koneksi.php";
      //  $sql = mysqli_query($db,"SELECT * FROM Pesanan");
      //  $count = mysqli_num_rows($sql);
      //  echo "$count"; ?></div>
     </div>
     <div class="col-auto">
       <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
     </div>
   </div>
 </div>
</div>
</div> -->

    
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div><a href="?page=lihatbarang" class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data Barang </a></div>
                      <?php 
       include "../koneksi/koneksi.php";
       $sql = mysqli_query($db,"SELECT * FROM pesanan");
       $count = mysqli_num_rows($sql); ?>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo ": $count"; ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div><a href="?page=reseller" class="text-xs font-weight-bold text-success text-uppercase mb-1">Data Reseller</a></div>
                      <?php 
       include "../koneksi/koneksi.php";
       $sql = mysqli_query($db,"SELECT * FROM reseller");
       $count = mysqli_num_rows($sql); ?>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo ": $count"; ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div><a href="?page=lihathasil" class="text-xs font-weight-bold text-info text-uppercase mb-1">Produksi</a></div>
                      <div class="row no-gutters align-items-center">
                      <?php 
       include "../koneksi/koneksi.php";
       $sql = mysqli_query($db,"SELECT * FROM hasil_produksi");
       $count = mysqli_num_rows($sql); ?>
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo ": $count"; ?></div>
                        </div>
                        <div class="col">
                          <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div><a href="?page=lihatpesanan"  class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pesanan</a></div>
                      <?php 
       include "../koneksi/koneksi.php";
       $sql = mysqli_query($db,"SELECT * FROM pesanan");
       $count = mysqli_num_rows($sql); ?>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo ": $count"; ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

  
       
        

    