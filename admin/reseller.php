<html>
<head>
  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
 
 
 <script src="../js/jquery-3.1.0.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
 

</head>
<body>
	 <!-- Begin PNama User Content -->
 <div class="container-fluid">

<!-- PNama User Heading -->
<h1 class="h3 mb-2 text-gray-800">Data Reseller</h1>
<p class="mb-2">
	<div class="my-2"></div>
                  <a href="index.php?page=tambahreseller" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-plus"></i>
                    </span>
                    <span class="text" >Tambah Data</span>
				  </a>
				  <a href="index.php?page=detailreseller" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-table"></i>
                    </span>
                    <span class="text" >Detail Deposit</span>
				  </a>
</p>


<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Data reseller</h6>
  </div>
  <div class="card-body">
	<div class="table-responsive">
	
	  <table class="table table-hover" id="example" width="100%" cellspacing="0">
	  <caption>Data reseller</caption>
		<thead>
		  <tr>
			<th>No</th>
			<th>Id Reseller</th>
			<th>Nama Reseller</th>
			<th>Alamat</th>
			<th>Nama Olshop</th>
			<th>Deposit</th>
			<th>Action</th>
		  </tr>
		</thead>
		<tfoot>
                <tr>
            <th>No</th>
			<th>Id Reseller</th>
			<th>Nama Reseller</th>
			<th>Alamat</th>
			<th>Nama Olshop</th>
			<th>Deposit</th>
			<th>Action</th>
                </tr>
                  </tfoot>
                  <tbody>
				  <?php 
			//untuk meinclude kan koneksi
			include('../koneksi/koneksi.php');

				//jika kita klik cari, maka yang tampil query cari ini
				if(isset($_POST['cari'])) {
					//menampung variabel kata_cari dari form pencarian
					$cari = $_POST['cari'];

					//jika hanya ingin mencari berdasarkan kode_produk, silahkan hapus dari awal OR
					//jika ingin mencari 1 ketentuan saja query nya ini : SELECT * FROM produk WHERE kode_produk like '%".$kata_cari."%' 
					$query = "SELECT * FROM reseller ORDER BY id_reseller ASC";
				} else {
					//jika tidak ada pencarian, default yang dijalankan query ini
					$query = "SELECT * FROM reseller ORDER BY id_reseller ASC";
				}
				$no = 1;

				$result = mysqli_query($db, $query);

				if(!$result) {
					die("Query Error : ".mysqli_errno($db)." - ".mysqli_error($db));
				}
				//kalau ini melakukan foreach atau perulangan
				while ($row = mysqli_fetch_assoc($result)) {
			
						//menampilkan data perulangan
						echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$row['id_reseller'].'</td>
							<td>'.$row['nm_reseller'].'</td>
							<td>'.$row['alamat'].'</td>
							<td>'.$row['nm_olshop'].'</td>
							<td>'.$row['deposit'].'</td>
							<td>
							<a href="index.php?page=tambahdeposit&id_reseller='.$row['id_reseller'].'" class="badge badge-primary">Tambah Deposit</a>
								<a href="index.php?page=editreseller&id_reseller='.$row['id_reseller'].'" class="badge badge-warning">Edit</a>
								<a href="reseller_hapus.php?id_reseller='.$row['id_reseller'].'" class="badge badge-danger" onclick="return confirm(\'Yakin ingin menghapus data ini?\')">Delete</a>
							</td>
						</tr>
						';
						$no++;
					}
				//jika query menghasilkan nilai 0
	
				?>
		</tbody>
		<tfoot>
	  </table>
  

<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>



	</div>
  </div>

</div>


  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page jenis_reseller plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page jenis_reseller custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
				</body>
				<html>