
<?php
include "../koneksi/koneksi.php";
		//jika sudah mendapatkan parameter GET id dari URL
		if(isset($_GET['id_reseller'])){
			//membuat variabel $id untuk menyimpan id dari GET id di URL
			$id_reseller = $_GET['id_reseller'];
			
			//query ke database SELECT tabel mahasiswa berdasarkan id = $id
			$select = mysqli_query($db, "SELECT * FROM reseller WHERE id_reseller='$id_reseller'") or die(mysqli_error($db));
			
			//jika hasil query = 0 maka muncul pesan error
			if(mysqli_num_rows($select) == 0){
				echo '<div class="alert alert-warning">Data tidak ada dalam database.</div>';
				exit();
			//jika hasil query > 0
			}else{
				//membuat variabel $data dan menyimpan data row dari query
				$data = mysqli_fetch_assoc($select);
			}
		}
		?>
		
		<?php
		include "../koneksi/koneksi.php";
		//jika tombol simpan di tekan/klik
		if(isset($_POST['submit'])){
			$id_reseller			= $_POST['id_reseller'];
			$nm_reseller			= $_POST['nm_reseller'];
			$alamat			= $_POST['alamat'];
			$nm_olshop					= $_POST['nm_olshop'];
			$deposit					= $_POST['deposit'];
			
			
			$sql = mysqli_query($db, "UPDATE reseller SET nm_reseller='$nm_reseller',alamat='$alamat',nm_olshop='$nm_olshop',deposit='$deposit'  WHERE id_reseller='$id_reseller'") or die(mysqli_error($db));
			
			if($sql){
				echo '<script>alert("Berhasil mengedit data."); document.location="index.php?page=reseller";</script>';
			}else{
				echo '<div class="alert alert-warning">Gagal melakukan proses edit data.</div>';
			}
		}
		?>
		
<!-- Begin Pnm_reseller User Content -->
<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Edit Reseller</h6>
  </div>
  <div class="card-body">


<div class="container" style="margin-top:5px">

		<form action="reseller_edit.php?id_reseller=<?php echo $id_reseller; ?>" method="post">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Id Reseller</label>
				<div class="col-sm-9">
					<input type="text" name="id_reseller" class="form-control" value="<?php echo $data['id_reseller']; ?>" size="4"  readonly >
				</div>
			</div>
		
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Nama Reseller </label>
				<div class="col-sm-9">
					<input type="text" name="nm_reseller" class="form-control" value="<?php echo $data['nm_reseller']; ?>" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Alamat </label>
				<div class="col-sm-9">
					<input type="text" name="alamat" class="form-control" value="<?php echo $data['alamat']; ?>" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Nama Olshop</label>
				<div class="col-sm-9">
					<input type="text" name="nm_olshop" class="form-control" value="<?php echo $data['nm_olshop']; ?>" require>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Deposit</label>
				<div class="col-sm-9">
					<input type="text" name="deposit" class="form-control" value="<?php echo $data['deposit']; ?>" require>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">&nbsp;</label>
				<div class="col-sm-10">
					<input type="submit" name="submit" class="btn btn-primary" value="SIMPAN">
					<a href="index.php?page=reseller" class="btn btn-warning">KEMBALI</a>
				</div>
			</div>
		</form>
	
	</div>
	</div>
	</div>
	</div>
	
	 
	<script src="js/jquery-3.3.1.slim.min.js" ></script>
	<script src="js/popper.min.js" ></script>
	<script src="js/bootstrap.min.js" ></script>

</body>



