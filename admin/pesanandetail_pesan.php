<?php
include "../koneksi/koneksi.php";
		//jika sudah mendapatkan parameter GET id dari URL
		?>
<html>
<head>
  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->


</head>
<body>
	 <!-- Begin PNama User Content -->
 <div class="container-fluid">

<!-- PNama User Heading -->
<h1 class="h3 mb-2 text-gray-800">Data Detail Barang Pesanan</h1>

<p class="mb-2">
	<div class="my-2"></div>
                  <a href="index.php?page=lihatpesanan" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text" >Kembali</span>
				  </a>
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Data Detail Barang Pesanan</h6>
  </div>
  <div class="card-body">
	<div class="table-responsive">
	
	  <table class="table table-hover" id="example" width="100%" cellspacing="0">
	  <caption>Data Detail Pesanan</caption>
		<thead>
		  <tr>
			<th>No</th>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Qty</th>
			<th>Harga Barang</th>
			<th>SubTotal</th>
			<th>Id Session</th>
		  </tr>
		</thead>
                  <tbody>
				  <?php 
				  include "../koneksi/koneksi.php";
				  //jika sudah mendapatkan parameter GET id dari URL
				  if(isset($_GET['id_session']) && $_GET['id_session']){
					  //membuat variabel $id untuk menyimpan id dari GET id di URL
					  $id_session = $_GET['id_session'];
                      $xid_session = $_GET['id_session'];
                      
					  
					  //query ke database SELECT tabel mahasiswa berdasarkan id = $id
					//   $select = mysqli_query($db, "SELECT * FROM pesanan_detail WHERE id_session='$id_session'") or die(mysqli_error($db));
            $select = mysqli_multi_query ($db, "SELECT * FROM pesanan_detail WHERE id_session='$id_session'") or die(mysqli_error($db));
		// $select = mysqli_query($db, "SELECT id_session='$id_session', COUNT(id_session) AS count FROM pesanan_detail GROUP BY id_session HAVING COUNT(id_session) > 1;") or die(mysqli_error($db));
		
					  //jika hasil query = 0 maka muncul pesan error
					  if(mysqli_num_rows($select) == 0){ 
						  echo '<div class="alert alert-warning">Data tidak ada dalam database.</div>';
					  //jika hasil query > 0
					  }else{
						  //membuat variabel $data dan menyimpan data row dari query
						  $data = mysqli_fetch_assoc($select);
					  }
                    
				  
				//   include('../koneksi/koneksi.php');
				//   if (isset($_GET['id_session'])) {
				// 	$id_session=trim($_GET['id_session']);
				// 	$sql="select * from pesanan_detail where id_session='$id_session' order by idpesanan asc";
				// }else {
				// 	$sql="select * from pesanan_detail order by idpesanan asc";
				// }
		
		
				// $hasil=mysqli_query($db,$sql);
				$no=0;
				// while ($data = mysqli_fetch_array($hasil)) {
					$no++;
			// //untuk meinclude kan koneksi
			// include('../koneksi/koneksi.php');
		
			// 	//jika kita klik cari, maka yang tampil query cari ini
			// 	if(isset($_GET['id_session'])){
			// 		//membuat variabel $id untuk menyimpan id dari GET id di URL
			// 		$id_session = $_GET['id_session'];

			// 		//jika hanya ingin mencari berdasarkan kode_produk, silahkan hapus dari awal OR
			// 		//jika ingin mencari 1 ketentuan saja query nya ini : SELECT * FROM produk WHERE kode_produk like '%".$kata_cari."%' 
			// 		$query = "SELECT * FROM pesanan_detail WHERE id_session = '$id_session'";
			// 	//jika hasil query = 0 maka muncul pesan error
			// 	}else{
			// 	//membuat variabel $data dan menyimpan data row dari query
			// 	$data = mysqli_fetch_assoc($query);

						
                    //menampilkan data perulangan
                   
						echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$data['kode_barang'].'</td>
							<td>'.$data['nama_barang'].'</td>
							<td>'.$data['qty'].'</td>
							<td>'.$data['harga_barang'].'</td>
							<td>'.$data['subtotal'].'</td>
							<td>'.$data['id_session'].'</td>
						</tr>
						';
						$no++;
					}
				
				//jika query menghasilkan nilai 0
	
				?>
				
		</tbody>
		<tfoot>
	  </table> 
	  <td colspan="5" align="center">Total :</td>
<td><?php
			
					include "../koneksi/koneksi.php";
					$query = mysqli_query($db, "SELECT SUM(subtotal) AS total FROM pesanan_detail WHERE id_session='$id_session'") or die(mysqli_error());
					$fetch = mysqli_fetch_array($query);
			?>
				<label class="text-danger"><?php echo '<a> Rp.' . number_format($fetch['total'], 0, ".", ".").'</a>'?></label>
			</td>
<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>



	</div>
  </div>

</div>
</div>


				</body>
				<html>