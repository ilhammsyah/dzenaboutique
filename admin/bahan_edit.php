
<?php
include "../koneksi/koneksi.php";
		//jika sudah mendapatkan parameter GET id dari URL
		if(isset($_GET['kode_barang'])){
			//membuat variabel $id untuk menyimpan id dari GET id di URL
			$kode_barang = $_GET['kode_barang'];
			
			//query ke database SELECT tabel mahasiswa berdasarkan id = $id
			$select = mysqli_query($db, "SELECT * FROM barang WHERE kode_barang='$kode_barang'") or die(mysqli_error($db));
			
			//jika hasil query = 0 maka muncul pesan error
			if(mysqli_num_rows($select) == 0){
				echo '<div class="alert alert-warning">Data tidak ada dalam database.</div>';
				exit();
			//jika hasil query > 0
			}else{
				//membuat variabel $data dan menyimpan data row dari query
				$data = mysqli_fetch_assoc($select);
			}
		}
		?>
		
		<?php
		include "../koneksi/koneksi.php";
		//jika tombol simpan di tekan/klik
		if(isset($_POST['submit'])){
			$kode_barang			= $_POST['kode_barang'];
			$nama_barang			= $_POST['nama_barang'];
			$harga_barang			= $_POST['harga_barang'];
			$stok					= $_POST['stok'];
			
			
			$sql = mysqli_query($db, "UPDATE barang SET nama_barang='$nama_barang',harga_barang='$harga_barang'  WHERE kode_barang='$kode_barang'") or die(mysqli_error($db));
			
			if($sql){
				echo '<script>alert("Berhasil mengedit data."); document.location="index.php?page=lihatbarang";</script>';
			}else{
				echo '<div class="alert alert-warning">Gagal melakukan proses edit data.</div>';
			}
		}
		?>
		
<!-- Begin Pnama_barang User Content -->
<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Edit Barang</h6>
  </div>
  <div class="card-body">


<div class="container" style="margin-top:5px">

		<form action="barang_edit.php?kode_barang=<?php echo $kode_barang; ?>" method="post">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Kode Barang</label>
				<div class="col-sm-9">
					<input type="text" name="kode_barang" class="form-control" value="<?php echo $data['kode_barang']; ?>" size="4"  readonly >
				</div>
			</div>
		
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Nama Barang</label>
				<div class="col-sm-9">
					<input type="text" name="nama_barang" class="form-control" value="<?php echo $data['nama_barang']; ?>" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Harga Barang </label>
				<div class="col-sm-9">
					<input type="text" name="harga_barang" class="form-control" value="<?php echo $data['harga_barang']; ?>" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Stok Barang</label>
				<div class="col-sm-9">
					<input type="text" name="stok" class="form-control" value="<?php echo $data['stok']; ?>" readonly>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">&nbsp;</label>
				<div class="col-sm-10">
					<input type="submit" name="submit" class="btn btn-primary" value="SIMPAN">
					<a href="index.php?page=lihatbarang" class="btn btn-warning">KEMBALI</a>
				</div>
			</div>
		</form>
	
	</div>
	
	 
	<script src="js/jquery-3.3.1.slim.min.js" ></script>
	<script src="js/popper.min.js" ></script>
	<script src="js/bootstrap.min.js" ></script>

</body>
</div>


