
        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- 404 Error Text -->
            <div class="text-center">
              <div class="error mx-auto" data-text="404">404</div>
              <p class="lead text-gray-800 mb-5">Halaman Sedang Maintenance</p>
              <p class="text-gray-500 mb-0">Maaf Halaman yang anda tuju sedang dilakukan pemeriksaan fungsional, servis, perbaikan atau penggantian perangkat </p>
              <a href="index.php?page=home">&larr; Kembali ke Dashboard</a>
            </div>
  
          </div>
          <!-- /.container-fluid -->