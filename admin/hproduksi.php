<html>
<head>
  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
 
 
 <script src="../js/jquery-3.1.0.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
 

</head>
<body>
	 <!-- Begin PNama User Content -->
 <div class="container-fluid">

<!-- PNama User Heading -->
<h1 class="h3 mb-2 text-gray-800">Data Hasil Produksi</h1>
<p class="mb-2">
	<div class="my-2"></div>
                  <a href="index.php?page=tambahhasil" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-plus"></i>
                    </span>
                    <span class="text" >Tambah Data</span>
				  </a>
</p>


<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<h6 class="m-0 font-weight-bold text-primary">Data Hasil Produksi</h6>
  </div>
  <div class="card-body">
	<div class="table-responsive">
	
	  <table class="table table-hover" id="example" width="100%" cellspacing="0">
	  <caption>Data Hasil Produksi</caption>
		<thead>
		  <tr>
			<th>No</th>
			<th>Jahit</th>
			<th>Total</th>
			<th>Obras</th>
			<th>Total</th>
			<th>Overdeck</th>
			<th>Total</th>
			<th>Action</th>
		  </tr>
		</thead>
		<tfoot>
                <tr>
			<th>No</th>
			<th>Jahit</th>
			<th>Total</th>
			<th>Obras</th>
			<th>Total</th>
			<th>Overdeck</th>
			<th>Total</th>
			<th>Action</th>
                </tr>
                  </tfoot>
                  <tbody>
                  	 <?php 
			//untuk meinclude kan koneksi
			include('../koneksi/koneksi.php');

				//jika kita klik cari, maka yang tampil query cari ini
				if(isset($_POST['cari'])) {
					//menampung variabel kata_cari dari form pencarian
					$cari = $_POST['cari'];

					//jika hanya ingin mencari berdasarkan kode_produk, silahkan hapus dari awal OR
					//jika ingin mencari 1 ketentuan saja query nya ini : SELECT * FROM produk WHERE kode_produk like '%".$kata_cari."%' 
					$query = "SELECT * FROM hasil_produksi WHERE id like '%".$cari."%' OR jahit like '%".$cari."%' OR obras like '%".$cari."%' ORDER BY id ASC";
				} else {
					//jika tidak ada pencarian, default yang dijalankan query ini
					$query = "SELECT * FROM hasil_produksi ORDER BY id ASC";
				}
				$no = 1;

				$result = mysqli_query($db, $query);

				if(!$result) {
					die("Query Error : ".mysqli_errno($db)." - ".mysqli_error($db));
				}
				//kalau ini melakukan foreach atau perulangan
				while ($row = mysqli_fetch_assoc($result)) {
			
						//menampilkan data perulangan
						echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$row['jahit'].'</td>
							<td>'.$row['total_jahit'].'</td>
							<td>'.$row['obras'].'</td>
							<td>'.$row['total_obras'].'</td>
							<td>'.$row['overdeck'].'</td>
							<td>'.$row['total_overdeck'].'</td>
							<td>
								<a href="hproduksi_hapus.php?id='.$row['id'].'" class="badge badge-danger" onclick="return confirm(\'Yakin ingin menghapus data ini?\')">Delete</a>
							</td>
						</tr>
						';
						$no++;
					}
				//jika query menghasilkan nilai 0
	
				?>
		</tbody>
		<tfoot>
	  </table>
  

<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>



	</div>
  </div>

</div>


  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page jenis_barang plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page jenis_barang custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
				</body>
				<html>