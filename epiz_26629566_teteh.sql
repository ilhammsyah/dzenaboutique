-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: sql212.epizy.com
-- Generation Time: Sep 07, 2020 at 05:55 AM
-- Server version: 5.6.48-88.0
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiz_26629566_teteh`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nama_toko` varchar(20) NOT NULL,
  `jenis_bahan` varchar(20) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `ttl` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `orderan` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(50) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `stok` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `stok`) VALUES
('A001', 'JAKET', 70000, 2),
('A002', 'Baju', 10000, 8);

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id` int(11) NOT NULL,
  `pesanan` varchar(20) NOT NULL,
  `stelan` varchar(25) NOT NULL,
  `baju` int(11) DEFAULT NULL,
  `celana` int(11) DEFAULT NULL,
  `rok` int(11) DEFAULT NULL,
  `pensil` int(11) DEFAULT NULL,
  `stdr_saku` int(11) DEFAULT NULL,
  `cln` int(11) DEFAULT NULL,
  `trp` int(11) DEFAULT NULL,
  `krd` int(11) DEFAULT NULL,
  `carok` int(11) DEFAULT NULL,
  `jaket` int(11) DEFAULT NULL,
  `zoger` int(11) DEFAULT NULL,
  `total` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_reseller`
--

CREATE TABLE `detail_reseller` (
  `id_session` int(11) NOT NULL,
  `nm_reseller` varchar(35) NOT NULL,
  `jenis_bahan` varchar(35) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_produksi`
--

CREATE TABLE `hasil_produksi` (
  `id` int(11) NOT NULL,
  `jahit` varchar(20) NOT NULL,
  `total_jahit` int(11) NOT NULL,
  `obras` varchar(20) NOT NULL,
  `total_obras` int(11) NOT NULL,
  `overdeck` varchar(20) NOT NULL,
  `total_overdeck` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `no_resi` varchar(35) NOT NULL,
  `idpesanan` int(11) NOT NULL,
  `nm_pemesan` varchar(40) NOT NULL,
  `total` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_session` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`no_resi`, `idpesanan`, `nm_pemesan`, `total`, `tanggal`, `id_session`) VALUES
('PSN20200906002', 6, 'ilham2', 10000, '2020-09-06', 'B000000000000009'),
('PSN20200906001', 5, 'ilham', 10000, '2020-09-06', 'B000000000000008'),
('PSN20200906003', 7, 'ilham3', 140000, '2020-09-06', 'B000000000000010'),
('PSN20200906004', 8, 'qq', 140000, '2020-09-06', 'B000000000000011'),
('PSN20200906005', 9, 'rr', 140000, '2020-09-06', 'B000000000000011'),
('PSN20200907006', 10, 'ilham', 140000, '2020-09-07', 'B000000000000012'),
('PSN20200907007', 11, 'ilham', 70000, '2020-09-07', 'B000000000000013');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan_detail`
--

CREATE TABLE `pesanan_detail` (
  `kode_barang` varchar(30) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `id_session` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pesanan_detail`
--

INSERT INTO `pesanan_detail` (`kode_barang`, `nama_barang`, `qty`, `harga_barang`, `subtotal`, `id_session`) VALUES
('A002', 'JAKET', 1, 100000, 100000, 'B000000000000001'),
('A008', 'HOODIE', 1, 120000, 120000, 'B000000000000001'),
('A003', 'JEANS', 1, 200000, 200000, 'B000000000000002'),
('A003', 'JEANS', 1, 200000, 200000, 'B000000000000003'),
('A008', 'HOODIE', 1, 120000, 120000, 'B000000000000004'),
('A002', 'Celana', 12, 70000, 840000, 'B000000000000005'),
('A008', 'JEANS', 10, 120000, 1200000, 'B000000000000006'),
('A001', 'JAKET', 15, 6000, 90000, 'B000000000000007'),
('A002', 'Baju', 1, 10000, 10000, 'B000000000000008'),
('A002', 'Baju', 1, 10000, 10000, 'B000000000000009'),
('A002', 'Baju', 2, 10000, 20000, 'B000000000000010'),
('A001', 'JAKET', 2, 70000, 140000, 'B000000000000010'),
('A002', 'Baju', 1, 10000, 10000, 'B000000000000011'),
('A001', 'JAKET', 2, 70000, 140000, 'B000000000000011'),
('A002', 'Baju', 2, 10000, 20000, 'B000000000000012'),
('A001', 'JAKET', 2, 70000, 140000, 'B000000000000012'),
('A002', 'Baju', 1, 10000, 10000, 'B000000000000013'),
('A001', 'JAKET', 1, 70000, 70000, 'B000000000000013');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan_temp`
--

CREATE TABLE `pesanan_temp` (
  `kode_barang` varchar(30) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `id_session` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reseller`
--

CREATE TABLE `reseller` (
  `id_reseller` varchar(11) NOT NULL,
  `nm_reseller` varchar(45) NOT NULL,
  `alamat` text NOT NULL,
  `nm_olshop` varchar(45) NOT NULL,
  `deposit` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reseller_detail`
--

CREATE TABLE `reseller_detail` (
  `id_session` varchar(30) NOT NULL,
  `id_reseller` int(11) NOT NULL,
  `jenis_bahan` varchar(45) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reseller_detail`
--

INSERT INTO `reseller_detail` (`id_session`, `id_reseller`, `jenis_bahan`, `qty`, `harga`, `jumlah`) VALUES
('C000000000000001', 0, 'Celana', 2, 10000, 20000),
('C000000000000001', 0, 'Baju', 2, 10000, 20000),
('C000000000000002', 0, 'Celana', 1, 90, 90),
('C000000000000002', 0, 'Celanaa', 1, 902, 902),
('C000000000000002', 0, 'baju', 1, 9022, 9022),
('C000000000000003', 959, 'Celana', 4, 10000, 40000),
('C000000000000003', 1008, 'Celanaa', 4, 100001, 400004),
('C000000000000004', 1030, 'a', 1, 1, 1),
('C000000000000004', 1038, 'av', 1, 11, 11),
('C000000000000005', 1, 'v', 1, 12, 12),
('C000000000000005', 2, 'vs', 11, 12, 132),
('C000000000000006', 0, 'baju', 12, 12, 144),
('C000000000000007', 0, 'katun', 200, 10000, 2000000),
('C000000000000008', 0, '123', 1, 1, 1),
('C000000000000009', 0, '123', 1, 1, 1),
('C000000000000010', 0, '123', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reseller_save`
--

CREATE TABLE `reseller_save` (
  `id_session` varchar(30) NOT NULL,
  `id_reseller` varchar(11) NOT NULL,
  `nm_reseller` varchar(35) NOT NULL,
  `nm_olshop` varchar(45) NOT NULL,
  `alamat` text NOT NULL,
  `deposit` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reseller_temp`
--

CREATE TABLE `reseller_temp` (
  `id_session` varchar(30) NOT NULL,
  `id_reseller` varchar(30) NOT NULL,
  `jenis_bahan` varchar(30) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stokreseller`
--

CREATE TABLE `stokreseller` (
  `id_stok` int(11) NOT NULL,
  `id_reseller` varchar(11) NOT NULL,
  `deposit` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `stokreseller_keluar`
--

CREATE TABLE `stokreseller_keluar` (
  `id` int(11) NOT NULL,
  `idpesanan` int(11) NOT NULL,
  `deposit` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `stok_keluar`
--

CREATE TABLE `stok_keluar` (
  `id` int(11) NOT NULL,
  `kode_barang` varchar(30) NOT NULL,
  `idpesanan` int(11) NOT NULL,
  `stok_keluar` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_keluar`
--

INSERT INTO `stok_keluar` (`id`, `kode_barang`, `idpesanan`, `stok_keluar`, `tanggal`) VALUES
(3, 'A002', 0, 9, '2020-09-06'),
(4, 'A002', 6, 8, '2020-09-06'),
(5, 'A001', 7, 3, '2020-09-06'),
(6, 'A001', 8, 1, '2020-09-06'),
(7, 'A001', 9, 9, '2020-09-06'),
(8, 'A001', 10, 7, '2020-09-06'),
(9, 'A001', 9, 5, '2020-09-06'),
(10, 'A001', 10, 3, '2020-09-07'),
(11, 'A001', 11, 2, '2020-09-07');

-- --------------------------------------------------------

--
-- Table structure for table `stok_masuk`
--

CREATE TABLE `stok_masuk` (
  `id` int(20) NOT NULL,
  `kode_barang` varchar(35) NOT NULL,
  `stok_masuk` int(20) NOT NULL,
  `tanggal_masuk` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_masuk`
--

INSERT INTO `stok_masuk` (`id`, `kode_barang`, `stok_masuk`, `tanggal_masuk`) VALUES
(15, 'A001', 10, '2020-09-08'),
(14, 'A001', 5, '2020-09-06'),
(13, 'A002', 10, '2020-09-07');

-- --------------------------------------------------------

--
-- Table structure for table `stok_masuk1`
--

CREATE TABLE `stok_masuk1` (
  `stok_masuk` int(11) NOT NULL,
  `tanggal_masuk` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` enum('admin','pegawai') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 'admin2020', 'admin'),
(2, 'pegawai 1', 'pegawai', 'pegawai2020', 'pegawai');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_produksi`
--
ALTER TABLE `hasil_produksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`idpesanan`);

--
-- Indexes for table `pesanan_temp`
--
ALTER TABLE `pesanan_temp`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `reseller`
--
ALTER TABLE `reseller`
  ADD PRIMARY KEY (`id_reseller`);

--
-- Indexes for table `reseller_save`
--
ALTER TABLE `reseller_save`
  ADD PRIMARY KEY (`id_reseller`);

--
-- Indexes for table `stokreseller`
--
ALTER TABLE `stokreseller`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `stokreseller_keluar`
--
ALTER TABLE `stokreseller_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok_keluar`
--
ALTER TABLE `stok_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok_masuk`
--
ALTER TABLE `stok_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok_masuk1`
--
ALTER TABLE `stok_masuk1`
  ADD PRIMARY KEY (`stok_masuk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hasil_produksi`
--
ALTER TABLE `hasil_produksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `idpesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `stokreseller`
--
ALTER TABLE `stokreseller`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `stokreseller_keluar`
--
ALTER TABLE `stokreseller_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stok_keluar`
--
ALTER TABLE `stok_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `stok_masuk`
--
ALTER TABLE `stok_masuk`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
